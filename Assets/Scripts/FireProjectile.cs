﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireProjectile : MonoBehaviour
{
    public Rigidbody projectile;
    public float projectileForce ;
    public string fireButton = "Fire1";
    public ForceMode ForceMode;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown(fireButton)) {
            Rigidbody instance = Instantiate(projectile, transform.position, Quaternion.identity);
            instance.AddForce(transform.forward*projectileForce, ForceMode);
        }
    }
}
