﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnWall : MonoBehaviour
{
    public int width = 20;
    public int height = 10;
    public GameObject brick;
    private Vector3 brickPosition;
    private float brickWidth;
    private float brickHeight;
    private float rowOffset = 0;
    private float incrementingColor1 = 0;
    private float incrementingColor2 = 0;
    private float incrementingColor3 = 0;

    // Start is called before the first frame update
    void Start()
    {
        brickWidth = brick.transform.localScale.x;
        brickHeight = brick.transform.localScale.y;
        for (int y = 0; y<height; y++) {
            incrementingColor1 = (float)(y+1)*10/100; 
            incrementingColor2 = (float)(y+1)*2/100;
            incrementingColor3 = (float)(y+1)*5/100;
            Debug.Log(incrementingColor1);
            Debug.Log(incrementingColor2);
            Debug.Log(incrementingColor3);

            for (int x = 0; x < width; x++) {
                if (y % 2 == 0) {
                    rowOffset = brickWidth * .5f;
                } else {
                    rowOffset = 0;
                }
                GameObject objInstance = Instantiate(brick, transform.position + x*(transform.right * (brickWidth)) + transform.right*rowOffset + y*(transform.up * (brickHeight)), Quaternion.identity);

                //Randomly changes objects material color
                //Color rndColor = Random.Range(0,2) == 1 ? Color.black : Color.white;
                
                objInstance.GetComponent<MeshRenderer>().material.color = new Color(incrementingColor1, incrementingColor2, incrementingColor3, Random.Range(0f, 1f));
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
